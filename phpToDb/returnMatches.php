<?php

    //create connection
    $servername = "localhost";
    $dbusername = "sthrashc_admin";
    $dbpassword = "admin";
    $dbname = "sthrashc_letsGameDb";
    
    $username = $_POST['a'];
    
    $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
    
    //check connection
    if ($conn -> connect_error)
    {
        die("Connection failed: " . $conn -> connect_error);
    }
    
    $sql = "SELECT playstyle, experience FROM profiles WHERE username = '$username';";
    $result = $conn -> query($sql);
    
    $records = array();
    
    if($result -> num_rows > 0)
    {
       //output data of each row
       while($row = $result->fetch_assoc())
       {
           $playstyle = $row['playstyle'];
           $experience = $row['experience'];
       }
    }
    
    $playstyleMin = $playstyle - 1;
    $playstyleMax = $playstyle + 1;
    $experienceMin = $experience - 1;
    $experienceMax = $experience + 1;
    
    $sql2 = "SELECT username FROM profiles WHERE (playstyle BETWEEN '$playstyleMin' AND '$playstyleMax') AND (experience BETWEEN '$experienceMin' AND '$experienceMax');";
    $matches = $conn -> query($sql2);
    
    if($matches -> num_rows > 0)
    {
       //output data of each row
       while($row = $matches->fetch_assoc())
       {
           if ($row['username'] != $username)
           {
                $records[]=$row['username'];
           }
       }
    }
    
    echo json_encode($records);
    
    $conn -> close();

?>