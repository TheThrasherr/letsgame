//
//  MatchDetailsViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 2/27/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

let playstyleLevels = ["Casual (1)","Fairly Casual (2)","Neutral (3)","Fairly Competitive (4)", "Competitive (5)"]
let experienceLevels = ["Novice (1)","Amateur (2)","Intermediate (3)","Experienced (4)","Veteran (5)"]

class MatchDetailsViewController: UIViewController {

    var values:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        
        print("\(match)")
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnProfile.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(match)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            
            self.values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            
            print("\(self.values)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            
            DispatchQueue.main.async
            {
                let mainData = self.values[0] as? [String:Any]
                
                self.lblGamertag?.text = mainData?["gamertag"] as? String
                self.lblBio?.text = mainData?["bio"] as? String
                self.lblPlaystyle?.text = playstyleLevels[Int((mainData?["playstyle"] as? String)!)! - 1]
                self.lblExperience?.text = experienceLevels[Int((mainData?["experience"] as? String)!)! - 1]
                
            } //end dispatch
            
        }//end task
        
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBOutlet weak var lblGamertag: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var lblPlaystyle: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    
}
