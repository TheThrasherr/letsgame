//
//  ModeratorViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/23/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class ModeratorViewController: UIViewController {

    @IBOutlet weak var btnPosts: UIButton!
    @IBOutlet weak var btnReports: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnPosts.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.btnReports.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLogout(_ sender: Any)
    {
       self.navigationController?.popToRootViewController(animated: true)
    }

}
