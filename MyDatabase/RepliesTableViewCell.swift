//
//  RepliesTableViewCell.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class RepliesTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblReply: UILabel!

}
