//
//  OgPostViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class OgPostViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var btnDelete: UIButton!

    @IBAction func btnDelete(_ sender: Any)
    {
        let alertController = UIAlertController(title: "Are you sure you would like to delete this post?", message: "Action cannot be undone", preferredStyle: UIAlertControllerStyle.alert)
        
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/removePost.php")! as URL)
            
            request.httpMethod = "POST"
            
            let postString = "a=\(reportedPost[0])&b=\(reportedPost[1])"
            
            request.httpBody = postString.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest)
            {data, response, error in
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                print("response=\(response)")
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString)")
                
                DispatchQueue.main.async
                    {
                        
                        if (responseString! == "Success")
                        {
                            let alertController = UIAlertController(title: "Post Successfully Removed", message: "Thanks for keeping it clean.", preferredStyle: UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Unable to Remove Post", message: "Please Try Again", preferredStyle: UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            
                            self.present(alertController,animated: true, completion: nil)
                        }
                } //end dispatch
                
            }//end task
            
            task.resume()
            
            
            let request2 = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/removeReportForPost.php")! as URL)
            
            request2.httpMethod = "POST"
            
            let postString2 = "a=\(reportedPost[0])&b=\(reportedPost[1])"
            
            request2.httpBody = postString2.data(using: String.Encoding.utf8)
            
            let task2 = URLSession.shared.dataTask(with: request2 as URLRequest)
            {data, response, error in
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                print("response=\(response)")
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString)")
                
                DispatchQueue.main.async
                {
                } //end dispatch
                
            }//end task
            
            task2.resume()

            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
        alertController.addAction(yesAction)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
        
        self.present(alertController, animated: true, completion:nil)
        
    }//end btnDelete
    
    var values:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnDelete.layer.borderColor = UIColor(red:255/255, green:0/255, blue:0/255, alpha: 1).cgColor
        
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        
        print("\(match)")
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/selectPost.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(reportedPost[0])&b=\(reportedPost[1])"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            
            self.values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            
            print("\(self.values)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            
            DispatchQueue.main.async
                {
                    let mainData = self.values[0] as? [String:Any]
                    
                    self.lblTitle?.text = mainData?["title"] as? String
                    self.lblUsername?.text = "Posted by: \((mainData?["username"] as? String)!)"
                    self.lblPost?.text = mainData?["description"] as? String

            } //end dispatch
            
        }//end task
        
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.btnDelete.layer.borderColor = UIColor(red:255/255, green:0/255, blue:0/255, alpha: 1).cgColor
    }


}
