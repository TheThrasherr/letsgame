//
//  AddReplyViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class AddReplyViewController: UIViewController {

    @IBOutlet weak var btnReply: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnReply.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var txtReply: UITextView!

    @IBAction func btnSubmitReply(_ sender: Any)
    {
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/insertReply.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(currentPost[0])&b=\(currentPost[1])&c=\(username)&d=\(txtReply.text!)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            DispatchQueue.main.async {
                
                if (responseString! == "Success")
                {
                    let alertController = UIAlertController(title: "New Reply", message: "Successfully Added", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: "Reply Failed", message: "Please Try Again", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController,animated: true, completion: nil)
                }
            } //end dispatch
            
        }//end task
        
        task.resume()
        
    }//end btnSubmitReply

}
