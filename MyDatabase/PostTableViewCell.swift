//
//  PostTableViewCell.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 11/28/17.
//  Copyright © 2017 COSC3326. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.btnReply.layer.borderColor = UIColor(red:0/255, green:255/255, blue:0/255, alpha: 1).cgColor
        
        self.btnReport.layer.borderColor = UIColor(red:255/255, green:0/255, blue:0/255, alpha: 1).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnReport: UIButton!

    @IBAction func btnReply(_ sender: Any)
    {
        if (username != "")
        {
            currentPost[0] = (lblTitle.text!)
            currentPost[1] = (lblUsername.text!)
        }
    }
    
    @IBAction func btnReport(_ sender: Any)
    {
        if (username != "")
        {
            currentPost[0] = (lblTitle.text!)
            currentPost[1] = (lblUsername.text!)
        }
    }
}
