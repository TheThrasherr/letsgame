//
//  ProfileViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 2/13/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    var values:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnProfile.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(username)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            
            self.values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            
            print("\(self.values)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            
            DispatchQueue.main.async
            {
                if self.values.isEqual(to: [])
                {
                    self.txtGamertag?.text = ""
                    self.txtvBio?.text = ""
                    self.sldPlaystyle?.value = 3.0
                    self.sldExperience?.value = 3.0
                }
                else
                {
                    let mainData = self.values[0] as? [String:Any]
                    
                    self.txtGamertag?.text = mainData?["gamertag"] as? String
                    self.txtvBio?.text = mainData?["bio"] as? String
                    self.sldPlaystyle?.value = Float((mainData?["playstyle"] as? String)!)!
                    self.sldExperience?.value = Float((mainData?["experience"] as? String)!)!
                }
                
            } //end dispatch
            
        }//end task
        
        task.resume()
        
        self.btnUpdate.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.btnMatches.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBOutlet weak var btnMatches: UIButton!
    @IBOutlet weak var txtGamertag: UITextField!
    @IBOutlet weak var txtvBio: UITextView!
    @IBOutlet weak var sldPlaystyle: UISlider!
    @IBOutlet weak var sldExperience: UISlider!

    @IBAction func btnUpdate(_ sender: Any) {
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/updateProfile.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(username)&b=\(txtGamertag.text!)&c=\(txtvBio.text!)&d=\(sldPlaystyle.value)&e=\(sldExperience.value)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            DispatchQueue.main.async {
                
                if (responseString! == "Success")
                {
                    let alertController = UIAlertController(title: "Profile Update", message: "Successfully Updated", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: "Cannot Update Profile", message: "Please Try Again", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController,animated: true, completion: nil)
                }
            } //end dispatch
            
        }//end task
        
        task.resume()

    }
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBAction func sldPlaystyleChanged(_ sender: UISlider) {
        sender.setValue(Float(lroundf(sender.value)), animated: true)
    }
    
    @IBAction func sldExperienceChanged(_ sender: UISlider) {
        sender.setValue(Float(lroundf(sender.value)), animated: true)
    }
    
}
