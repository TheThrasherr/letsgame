//
//  ReportViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {

    @IBOutlet weak var btnReport: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnReport.layer.borderColor = UIColor(red:255/255, green:0/255, blue:0/255, alpha: 1).cgColor
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var txtReport: UITextView!

    @IBAction func btnReport(_ sender: Any)
    {
        let alertController = UIAlertController(title: "Are you sure you would like to send a report?", message: "Action cannot be undone", preferredStyle: UIAlertControllerStyle.alert)
        
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/insertReport.php")! as URL)
            
            request.httpMethod = "POST"
            
            let postString = "a=\(currentPost[0])&b=\(currentPost[1])&c=\(username)&d=\(self.txtReport.text!)"
            
            request.httpBody = postString.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest)
            {data, response, error in
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                print("response=\(response)")
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString)")
                
                DispatchQueue.main.async
                {
                    
                    if (responseString! == "Success")
                    {
                        let alertController = UIAlertController(title: "User Reported", message: "Post under review", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else
                    {
                        let alertController = UIAlertController(title: "Report Failed", message: "Please Try Again", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alertController,animated: true, completion: nil)
                    }
                } //end dispatch
                
            }//end task
            
            task.resume()
            
        }
        alertController.addAction(yesAction)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive, handler: nil))
        
        self.present(alertController, animated: true, completion:nil)
        
    }//end btnReport


}
