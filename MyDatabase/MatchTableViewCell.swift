//
//  MatchTableViewCell.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 2/27/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class MatchTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var lblUsername: UILabel!
}
