//
//  ThirdViewController.swift
//  MyDatabase
//
//  Created by COSC3326 on 4/19/17.
//  Copyright © 2017 COSC3326. All rights reserved.
//

import UIKit

var currentPost = ["",""]

class ThirdViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        // Do any additional setup after loading the view.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var values:NSArray = []

    @IBOutlet weak var tblUsers: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PostTableViewCell
        
        let mainData = values[indexPath.row] as? [String:Any]
        
        cell.lblTitle?.text = mainData?["title"] as? String
        cell.lblDesc?.text = mainData?["description"] as? String
        cell.lblUsername?.text = mainData?["username"] as? String
        
        if (username == "")
        {
            cell.btnReply.isEnabled = false
            cell.btnReply.isHidden = true
            cell.btnReport.isEnabled = false
            cell.btnReport.isHidden = true
        }
        else
        {
            cell.btnReply.isEnabled = true
            cell.btnReply.isHidden = false
            cell.btnReport.isEnabled = true
            cell.btnReport.isHidden = false
        }

        
        return cell
    }
    
    func getData()
    {
            let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnPostsForGame.php")! as URL)
            
            request.httpMethod = "POST"
            
            let postString = "a=\(gameSelected[0])&b=\(gameSelected[1])"
            
            request.httpBody = postString.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest)
            {data, response, error in
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                print("response=\(response)")
                
                
                self.values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                
                print("\(self.values)")
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString!)")
                
                
                DispatchQueue.main.async
                    {
                        self.tblUsers.reloadData()
                } //end dispatch
                
            }//end task
            
            task.resume()

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        let currentCell = tableView.cellForRow(at: indexPath) as! PostTableViewCell!
        let postUsername = currentCell?.lblUsername.text
        
        if username == postUsername || username == "admin"
        {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            let currentCell = tableView.cellForRow(at: indexPath) as! PostTableViewCell!
            
            let title = (currentCell?.lblTitle.text)!!
            let postUsername = (currentCell?.lblUsername.text)!!
            
            if username != postUsername && username != "admin"
            {
                let alertController = UIAlertController(title: "Cannot Delete Post", message: "Posts can only be deleted by creator", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
                return
            }
            
            let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/removePost.php")! as URL)
            
            request.httpMethod = "POST"
            
            let postString = "a=\(title)&b=\(postUsername)"
            
            request.httpBody = postString.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest)
            {data, response, error in
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                print("response=\(response)")
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString)")
                
                DispatchQueue.main.async {
                    
                    if (responseString! == "Success")
                    {
                        let alertController = UIAlertController(title: "Post Removed", message: "Successfully Deleted", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        self.getData()
                    }
                    else
                    {
                        let alertController = UIAlertController(title: "Cannot Delete Post", message: "Error", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alertController,animated: true, completion: nil)
                    }
                } //end dispatch
                
            }//end task
            
            task.resume()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getData()
    }

}
