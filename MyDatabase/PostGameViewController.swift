//
//  PostGameViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/23/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class PostGameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        getData()
        
        // Do any additional setup after loading the view.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var values:NSArray = []
    
    @IBOutlet weak var tblGames: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GamesTableViewCell
        
        let mainData = values[indexPath.row] as? [String:Any]
        
        cell.lblTitle?.text = mainData?["title"] as? String
        cell.lblPlatform?.text = mainData?["platform"] as? String
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let indexPath = tableView.indexPathForSelectedRow
        
        let game = values[(indexPath?.row)!] as? [String:Any]
        
        newPost[0] = (game?["title"] as? String)!
        newPost[1] = (game?["platform"] as? String)!
        
    }
    
    func getData()
    {
        let url = NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnGames.php")
        let data = NSData(contentsOf: url! as URL)
        
        values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
        
        tblGames.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        getData()
    }

}
