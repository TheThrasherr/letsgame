//
//  SecondViewController.swift
//  MyDatabase
//
//  Created by COSC3326 on 4/19/17.
//  Copyright © 2017 COSC3326. All rights reserved.
//

import UIKit

//change password field to dots

class SecondViewController: UIViewController {

    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnCreateAcct: UIButton!
    @IBOutlet weak var btnGuest: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.isNavigationBarHidden = true
        
        self.btnSignIn.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.btnCreateAcct.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.btnGuest.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.txtUsername.text = ""
        self.txtPassword.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        self.txtUsername.text = ""
        self.txtPassword.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    @IBAction func btnGuest(_ sender: Any)
    {
        username = ""
    }
    
    @IBAction func btnSignIn(_ sender: Any)
    {
        /*if (txtUsername.text == "" || txtPassword.text == "")
        {
            //lblStatus.text = "* fields cannot be blank"
            return
        }*/
        
        let myURL = URL(string: "https://sthrash.create.stedwards.edu/letsGame/selectUser.php")
        var request = URLRequest(url: myURL!)
        
        request.httpMethod = "POST" //compose a query string
        
        let postString = "a=\(txtUsername.text!)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request){(data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil
            {
                print("error=\(error)")
                return
            }
            print("response= \(response)")
            
            do{
                let myJSON = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray
                
                if let parsed_data = myJSON
                {
                    DispatchQueue.main.async
                    {
                        if parsed_data.count == 0
                        {
                            let alertController = UIAlertController(title: "Access Denied", message: "Username does not exist", preferredStyle: UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            //self.lblStatus.text = "Unsuccessful Login"
                            self.txtUsername.text = ""
                            self.txtPassword.text = ""
                            return
                        }
                        
                        let userPass = parsed_data[0] as? NSDictionary
                        if(self.txtPassword.text == userPass!["password"]! as? String)
                        {
                            username = self.txtUsername.text!
                            //self.lblStatus.text = "Successful Login!"
                            if parsed_data.count == 0
                            {
                                let alertController = UIAlertController(title: "Welcome!", message: "Successfully Logged In", preferredStyle: UIAlertControllerStyle.alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                self.txtUsername.text = ""
                                self.txtPassword.text = ""
 
                            }
                            
                            if username == "admin"
                            {
                                self.performSegue(withIdentifier: "admin", sender: nil)
                            }
                            else
                            {
                                self.performSegue(withIdentifier: "login", sender: nil)
                            }
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Access Denied", message: "Incorrect Password", preferredStyle: UIAlertControllerStyle.alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            //self.lblStatus.text = "Unsuccessful Login"
                            self.txtPassword.text = ""

                        }
                    }
                }
            }
            catch
            {
                print(error)
            }
        }
        task.resume()
    }
    
}

