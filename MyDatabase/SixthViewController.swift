//
//  SixthViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 2/5/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class SixthViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var pickerData: [String] = [String]()
    var platform = ""

    @IBOutlet weak var btnAdd: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnAdd.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        // Do any additional setup after loading the view, typically from a nib.
        pickerData = ["PC", "XBOX", "PlayStation","Nintendo Switch","Wii U","General"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var pickPlatform: UIPickerView!
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    // Catpure the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        platform = pickerData[row]
        
    }
    
    //change color of picker text
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let str = pickerData[row]
        return NSAttributedString(string: str, attributes: [NSForegroundColorAttributeName:UIColor.white])
    }
    
    
    @IBAction func btnAddGame(_ sender: Any) {
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/insertGame.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(txtTitle.text!)&b=\(platform)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            DispatchQueue.main.async {
                
                if (responseString! == "Success")
                {
                    let alertController = UIAlertController(title: "New Game", message: "Successfully Added", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: "Cannot Create New Game", message: "Platform already exists for this title", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController,animated: true, completion: nil)
                }
            } //end dispatch
            
        }//end task
        
        task.resume()
        
    }//end btnSingUp
    
}
