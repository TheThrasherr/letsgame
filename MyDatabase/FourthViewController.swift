//
//  FourthViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 11/28/17.
//  Copyright © 2017 COSC3326. All rights reserved.
//

import UIKit

var newPost = ["",""]

class FourthViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.btnCreate.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var btnCreate: UIButton!
    
    
    @IBAction func btnCreatePost(_ sender: Any) {
        
        if (username == "")
        {
            let alertController = UIAlertController(title: "Cannot Create Post", message: "Must sign-in", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
            return
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/insertPost.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(txtTitle.text!)&b=\(txtDesc.text!)&c=\(username)&d=\(newPost[0])&e=\(newPost[1])"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            DispatchQueue.main.async {
                
                if (responseString! == "Success")
                {
                    let alertController = UIAlertController(title: "New Post", message: "Successfully Added", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: "Cannot Create New Post", message: "Title already exists for this username", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alertController,animated: true, completion: nil)
                }
            } //end dispatch
            
        }//end task
        
        task.resume()
        
    }//end btnSingUp

}
