//
//  MatchViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 2/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

var match = ""

class MatchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        getData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        getData()
    }
    
    var values:NSArray = []
    
    @IBOutlet weak var tblMatches: UITableView!
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! MatchTableViewCell
        
        match = currentCell.lblUsername.text!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MatchTableViewCell
        
        //let mainData = values[indexPath.row] as? [String:Any]
        cell.lblUsername?.text = values[indexPath.row] as? String
        
        return cell
    }
    
    func getData()
    {
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnMatches.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(username)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            
            self.values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            
            print("\(self.values)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            
            DispatchQueue.main.async
            {
                self.tblMatches.reloadData()
            } //end dispatch
            
        }//end task
        
        task.resume()

    }
    
    //send username to returnMatches.php
    //returnMatches.php returns matches of username
    
    
}
