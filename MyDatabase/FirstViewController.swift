//
//  FirstViewController.swift
//  MyDatabase
//
//  Created by COSC3326 on 4/19/17.
//  Copyright © 2017 COSC3326. All rights reserved.
//

import UIKit

var username = ""

class FirstViewController: UIViewController {

    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.btnSignUp.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtName: UITextField!

    @IBAction func btnSignUp(_ sender: Any) {
        
        if txtUsername.text! == "" || txtPassword.text! == "" || txtName.text! == ""
        {
            let alertController = UIAlertController(title: "Cannot Create Account", message: "Fields cannot be blank", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alertController,animated: true, completion: nil)
            
            return
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/insertUser.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(txtUsername.text!)&b=\(txtPassword.text!)&c=\(txtName.text!)"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            
            DispatchQueue.main.async {
                
                if (responseString! == "Success")
                {
                    let alertController = UIAlertController(title: "New Account", message: "Successfully Added", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: "Cannot Create New Account", message: "Username already exists", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                    self.present(alertController,animated: true, completion: nil)
                }
            } //end dispatch
            
        }//end task
        
        task.resume()
        
    }//end btnSingUp
    
}

