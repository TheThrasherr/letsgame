//
//  ViewReportsViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

var reportedPost = ["",""]

class ViewReportsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        getData()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        getData()
    }
    
    var values:NSArray = []
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
     
        let indexPath = tableView.indexPathForSelectedRow
        
        let post = values[(indexPath?.row)!] as? [String:Any]
     
        reportedPost[0] = (post?["title"] as? String)!
        reportedPost[1] = (post?["username"] as? String)!

     }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        //username = "admin"
        if username == "admin"
        {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            let currentCell = tableView.cellForRow(at: indexPath) as! ReportsTableViewCell!
            
            let reporter = (currentCell?.lblUsername.text)!!
            let reason = (currentCell?.lblReason.text)!!

            let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/removeReport.php")! as URL)
            
            request.httpMethod = "POST"
            
            let postString = "a=\(reportedPost[0])&b=\(reportedPost[1])&c=\(reporter)&d=\(reason)"
            
            request.httpBody = postString.data(using: String.Encoding.utf8)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest)
            {data, response, error in
                if error != nil
                {
                    print("error=\(error)")
                    return
                }
                
                print("response=\(response)")
                
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("responseString = \(responseString)")
                
                DispatchQueue.main.async {
                    
                    if (responseString! == "Success")
                    {
                        let alertController = UIAlertController(title: "Report Removed", message: "Successfully Deleted", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                        self.getData()
                    }
                    else
                    {
                        let alertController = UIAlertController(title: "Cannot Delete Report", message: "Please Try Again", preferredStyle: UIAlertControllerStyle.alert)
                        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        self.present(alertController,animated: true, completion: nil)
                    }
                } //end dispatch
                
            }//end task
            
            task.resume()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportsTableViewCell
        
        let mainData = values[indexPath.row] as? [String:Any]
        //print(values[indexPath.row])
        
        cell.lblUsername?.text = mainData?["reporter"] as? String
        cell.lblReason?.text = mainData?["reason"] as? String
        
        return cell
    }
    
    func getData()
    {
        let url = NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnReports.php")
        let data = NSData(contentsOf: url! as URL)
        
        values = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
        
        tblReports.reloadData()
        
    }

    @IBOutlet weak var tblReports: UITableView!
    

}
