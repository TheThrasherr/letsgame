//
//  RepliesViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class RepliesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        getData1()
        getData2()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        getData1()
        getData2()
    }
    
    var values1:NSArray = []
    var values2:NSArray = []
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! MatchTableViewCell
        
        match = currentCell.lblUsername.text!
    }*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RepliesTableViewCell
        
        //let mainData = values[indexPath.row] as? [String:Any]
        //print(values[indexPath.row])
        cell.lblUsername?.text = "Submitted by: \((values1[indexPath.row] as? String)!)"
        cell.lblReply?.text = values2[indexPath.row] as? String
        
        return cell
    }
    
    func getData1()
    {
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnRepliers.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(currentPost[0])&b=\(currentPost[1])"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            
            self.values1 = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            
            print("\(self.values1)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            
            
            DispatchQueue.main.async
                {
                    self.tblReplies.reloadData()
            } //end dispatch
            
        }//end task
        
        task.resume()
        
    }
    
    func getData2()
    {
        let request = NSMutableURLRequest(url: NSURL(string: "https://sthrash.create.stedwards.edu/letsGame/returnReplies.php")! as URL)
        
        request.httpMethod = "POST"
        
        let postString = "a=\(currentPost[0])&b=\(currentPost[1])"
        
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {data, response, error in
            if error != nil
            {
                print("error=\(error)")
                return
            }
            
            print("response=\(response)")
            
            
            self.values2 = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
            
            print("\(self.values2)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString!)")
            
            
            DispatchQueue.main.async
            {
                    self.tblReplies.reloadData()
            } //end dispatch
            
        }//end task
        
        task.resume()
        
    }

    @IBOutlet weak var tblReplies: UITableView!

}
