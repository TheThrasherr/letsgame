//
//  WelcomeViewController.swift
//  LetsGameDemo1
//
//  Created by COSC3326 on 3/22/18.
//  Copyright © 2018 COSC3326. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        lblWelcome.text = "Welcome, \(username)!"
        
        self.btnViewPosts.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        self.btnCreatePost.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        self.btnViewProfile.layer.borderColor = UIColor(red:255/255, green:205/255, blue:0/255, alpha: 1).cgColor
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLogout(_ sender: Any)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var btnViewPosts: UIButton!
    @IBOutlet weak var btnCreatePost: UIButton!
    @IBOutlet weak var btnViewProfile: UIButton!


}
